#--------------------------------------------------
# This code follows style guide:
# https://google.github.io/styleguide/Rguide.xml
#--------------------------------------------------

extractDrawsModel1 <- function(sample.results, samples) {
    # This will take the coda samples and extract the probabilites and
    # replicate data for each database for each draw.
    #
    # Args:
    #   sample.results: the output of the extractResultsModel1() function.
    #   samples: the coda samples
    #
    # Returns:
    #   A list of lists of extracted data. The top level lists correspond to the database.
    #   The inner lists contain the data from the coda samples
    #
    row.count <- nrow(sample.results[[1]])
    db.count <- length(sample.results)
    
    ret.val <- list()
    sample.matrix <- as.matrix(samples)
    
    for (db.index in 1:db.count) {
        
        lc.probs <- sample.matrix[, paste('prob[', db.index, ',', 1:row.count, ',1]', sep = '')]
        hc.probs <- sample.matrix[, paste('prob[', db.index, ',', 1:row.count, ',2]', sep = '')]
        lc.cases.rep <- sample.matrix[, paste('rep.data[', db.index, ',', 1:row.count, ',4]', sep = '')]
        hc.cases.rep <- sample.matrix[, paste('rep.data[', db.index, ',', 1:row.count, ',6]', sep = '')]
        
        list1 <- list(lc.probs = lc.probs,
                         hc.probs = hc.probs,
                         lc.cases.rep = lc.cases.rep,
                         hc.cases.rep = hc.cases.rep)
        

        
        ret.val[[db.index]] <- list1
    }
    
    return (ret.val)
}



extractDrawsModelAlt <- function(sample.results, samples) {
    # This will take the coda samples and extract the probabilites and
    # replicate data for each database for each draw.
    #
    # Args:
    #   sample.results: the output of the extractResultsModelAlt() function.
    #   samples: the coda samples
    #
    # Returns:
    #   A list of lists of extracted data. The top level lists correspond to the database.
    #   The inner lists contain the data from the coda samples
    #
    row.count <- nrow(sample.results[[1]])
    db.count <- length(sample.results)
    
    ret.val <- list()
    sample.matrix <- as.matrix(samples)
    
    for (db.index in 1:db.count) {
        
        probs <- sample.matrix[, paste('prob[', db.index, ',', 1:row.count, ']', sep = '')]
        cases.rep <- sample.matrix[, paste('rep.data[', db.index, ',', 1:row.count, ',4]', sep = '')]
        
        list1 <- list(probs = probs,
                      cases.rep = cases.rep)
        

        
        ret.val[[db.index]] <- list1
    }
    
    return (ret.val)
}



extractResultsModel1 <- function(samples, processed.data) {
    # This takes the sampled output from the JAGS model
    # and turns it into a list of data frames (one for
    # each database)
    #
    # Args:
    #   samples: the coda samples
    #   processed.data: the data output by the processData() function
    #
    # Returns:
    #   A list of data frames that include the posterior probabilites
    #   and replicate data.


    # This little helper function makes a name like "prob[1,3,4]"
    makeName <- function(name, db, row, col) {
        name <- paste(name,  '[', db, ',', row, ',', col, ']', sep = '' )
    }
    
    means <- summary(samples)$statistics[,"Mean"]
    ret.val <- list()
    
    for (db.index in 1:length(processed.data)) {
        
        db <- processed.data[[db.index]]
        age.group.count <- nrow(db)
        
        min.age.col <- rep(0, age.group.count)
        max.age.col <- rep(0, age.group.count)
        avg.age.col <- rep(0, age.group.count)
        lc.cases.col <- rep(0, age.group.count)
        lc.patients.col <- rep(0, age.group.count)
        lc.prob.col <- rep(0, age.group.count)
        hc.cases.col <- rep(0, age.group.count)
        hc.patients.col <- rep(0, age.group.count)
        hc.prob.col <- rep(0, age.group.count)
        lc.cases.rep.col <- rep(0, age.group.count)
        hc.cases.rep.col <- rep(0, age.group.count)

        for (age.group.index in 1:nrow(db)) {
            
            min.age.col[age.group.index] <- db$min.age[age.group.index]
            max.age.col[age.group.index] <- db$max.age[age.group.index]
            avg.age.col[age.group.index] <- db$avg.age[age.group.index]
            lc.cases.col[age.group.index] <- db$lc.cases[age.group.index]
            lc.patients.col[age.group.index] <- db$lc.patients[age.group.index]
            hc.cases.col[age.group.index] <- db$hc.cases[age.group.index]
            hc.patients.col[age.group.index] <- db$hc.patients[age.group.index]
            
            # Get probabilites. These are names "prob[m,n,o]" where 
            # m is the database, n is the age group, and o is cholesterol group
            lc.prob.name <- makeName('prob', db.index, age.group.index, 1)
            hc.prob.name <- makeName('prob', db.index, age.group.index, 2)
            
            lc.prob.col[age.group.index] <- means[lc.prob.name]
            hc.prob.col[age.group.index] <- means[hc.prob.name]
            
            # Get rep data. These names are rep.data[m,n,o] where
            # m is database, n is age group, and o = 4 for low chol heart disease cases, 
            # and o = 6 for high chol heart diseas cases
            lc.cases.rep.name <- makeName('rep.data', db.index, age.group.index, 4)
            hc.cases.rep.name <- makeName('rep.data', db.index, age.group.index, 6)
            
            lc.cases.rep.col[age.group.index] <- means[lc.cases.rep.name]
            hc.cases.rep.col[age.group.index] <- means[hc.cases.rep.name]
        }
        
        db.df <- data.frame(min.age = min.age.col,
                            max.age = max.age.col,
                            avg.age = avg.age.col,
                            lc.cases = lc.cases.col,
                            lc.patients = lc.patients.col,
                            lc.prob = lc.prob.col,
                            hc.cases = hc.cases.col,
                            hc.patients = hc.patients.col,
                            hc.prob = hc.prob.col,
                            lc.cases.rep = lc.cases.rep.col,
                            hc.cases.rep = hc.cases.rep.col)
        
        ret.val[[db.index]] <- db.df
    }
    
    return (ret.val)
}



extractResultsModelAlt <- function(samples, processed.data) {
    # This takes the sampled output from the JAGS model
    # and turns it into a list of data frames (one for
    # each database)
    #
    # Args:
    #   samples: the coda samples
    #   processed.data: the data output by the processDataAlt() function
    #
    # Returns:
    #   A list of data frames that include the posterior probabilites
    #   and replicate data.


    # This little helper function makes a name like "prob[1,3,4]"
    makeName <- function(name, db, row, col = NULL) {
        if (is.null(col)) {
            name <- paste(name,  '[', db, ',', row, ']', sep = '' )
            return (name);
        }

        name <- paste(name,  '[', db, ',', row, ',', col, ']', sep = '' )
        return (name);   
    }
    
    means <- summary(samples)$statistics[,"Mean"]
    ret.val <- list()
    
    for (db.index in 1:length(processed.data)) {
        
        db <- processed.data[[db.index]]
        age.group.count <- nrow(db)
        
        min.age.col <- rep(0, age.group.count)
        max.age.col <- rep(0, age.group.count)
        avg.age.col <- rep(0, age.group.count)
        cases.col <- rep(0, age.group.count)
        patients.col <- rep(0, age.group.count)
        prob.col <- rep(0, age.group.count)
        cases.rep.col <- rep(0, age.group.count)

        for (age.group.index in 1:nrow(db)) {
            
            min.age.col[age.group.index] <- db$min.age[age.group.index]
            max.age.col[age.group.index] <- db$max.age[age.group.index]
            avg.age.col[age.group.index] <- db$avg.age[age.group.index]
            cases.col[age.group.index] <- db$cases[age.group.index]
            patients.col[age.group.index] <- db$patients[age.group.index]
            
            # Get probabilites. These are names "prob[m,n,o]" where 
            # m is the database, n is the age group, and o is cholesterol group
            prob.name <- makeName('prob', db.index, age.group.index)

            prob.col[age.group.index] <- means[prob.name]
     
            
            # Get rep data. These names are rep.data[m,n,o] where
            # m is database, n is age group, and o = 4 for low chol heart disease cases, 
            # and o = 6 for high chol heart diseas cases
            cases.rep.name <- makeName('rep.data', db.index, age.group.index, 4)
            
            cases.rep.col[age.group.index] <- means[cases.rep.name]
        }
        
        db.df <- data.frame(min.age = min.age.col,
                            max.age = max.age.col,
                            avg.age = avg.age.col,
                            cases = cases.col,
                            patients = patients.col,
                            prob = prob.col,                        
                            cases.rep = cases.rep.col)
        
        ret.val[[db.index]] <- db.df
    }
    
    return (ret.val)
}



getTChiSqr <- function(sample.results, draws) {
    # Calculcate the chi-square discrepency
    #  
    # Args:
    #   sample.results: the output from extractResultsModel1() function
    #   draws: the output from extractDrawsModel1() function
    #
    # Returns:
    #   A list of vectors. Each vector contains the posterior predictive
    #   p-value for the overdispersion for low and high cholesterol. There is
    #   a vector for each database.
    
    db.count <- length(draws)
    ret.val <- list()
    
    calcTChi <- function(cases, patients, probs) {
        
        num <- (cases - patients*probs)^2
        den <- patients*probs*(1 - probs)
        
        val <- sum(num/den, na.rm = TRUE)
        
        return (val)
    }
    
    
    for (db.index in 1:db.count) {
        
        db.results <- sample.results[[db.index]]
        db.draws <- draws[[db.index]]
    
        
        draw.count <- nrow(db.draws[[1]])
        
        T.chi.lc <- numeric(draw.count)
        T.chi.hc <- numeric(draw.count)
        T.chi.rep.lc <- numeric(draw.count)
        T.chi.rep.hc <- numeric(draw.count)
        
        for (s in 1:draw.count) {
            
            # Do low chol first
            T.chi.lc[s] <- calcTChi(db.results$lc.cases, db.results$lc.patients, db.draws[['lc.probs']][s,])
            T.chi.rep.lc[s] <- calcTChi(db.draws$lc.cases.rep[s,], db.results$lc.patients, db.draws[['lc.probs']][s,])
            
            T.chi.hc[s] <- calcTChi(db.results$hc.cases, db.results$hc.patients, db.draws[['hc.probs']][s,])
            T.chi.rep.hc[s] <- calcTChi(db.draws$hc.cases.rep[s,], db.results$hc.patients, db.draws[['hc.probs']][s,])
        }
        
        ret.val[[db.index]] <- c(mean(T.chi.rep.lc >= T.chi.lc), mean(T.chi.rep.hc >= T.chi.hc))
    }
    
    return (ret.val)
}



init <- function () {
    # This intializes everything (e.g. load modules, libs, etc).
    # Only call this once.
    #
    # Args:
    #   none
    #
    # Returns:
    #   none

    library(rjags)
    load.module("glm")
}



initJagsModel1 <- function(jags.data) {
    # This will initialize the model.
    #
    # Args:
    #   jags.data: the data returned by makeJagsData()
    #
    # Returns:
    #   the model object.
    
    model.init <- list(list(beta.chol = c(10, 10), beta.age = 10, sigmaepsilon = 100),
                       list(beta.chol = c(-10, 10), beta.age = -10, sigmaepsilon = 0.01),
                       list(beta.chol = c(10, -10), beta.age = 10, sigmaepsilon= 100),
                       list(beta.chol = c(-10, -10), beta.age = -10, sigmaepsilon = 0.01))
    
    
    model.data <- list(dataset = jags.data)
    m1 <- jags.model("jags/model-2.bug", data = model.data, n.chains=4, n.adapt=1000)
    update(m1, 1000) 
    
    return (m1)
}



initJagsModelAlt <- function(jags.data) {
    # This will initialize the alternative model.
    #
    # Args:
    #   jags.data: the data returned by makeJagsData()
    #
    # Returns:
    #   the model object.
    
    model.init <- list(list(beta.age = 10, sigmaepsilon = 100),
                       list(beta.age = -10, sigmaepsilon = 0.01),
                       list(beta.age = 10, sigmaepsilon= 100),
                       list(beta.age = -10, sigmaepsilon = 0.01))
    
    
    model.data <- list(dataset = jags.data)
    m1 <- jags.model("jags/model-2-alt.bug", data = model.data, n.chains=4, n.adapt=1000)
    update(m1, 1000) 
    
    return (m1)
}



loadAllData <- function() {
    # This function will load the data into a list of data frames.
    # The loaded data will be minimally processed
    #
    # Args:
    #   none
    #
    # Returns:
    #   A list of the four databases.
    
    cleveland.data <- loadDataFromFile('data/processed.cleveland.data', use.factors = FALSE, use.booleans = FALSE)
    hungarian.data <- loadDataFromFile('data/processed.hungarian.data', use.factors = FALSE, use.booleans = FALSE)
    switzerland.data <- loadDataFromFile('data/processed.switzerland.data', use.factors = FALSE, use.booleans = FALSE)
    va.data <- loadDataFromFile('data/processed.va.data', use.factors = FALSE, use.booleans = FALSE)
    
    shiftFactors <- function(data) {
        data[, 'sex'] <- data[, 'sex'] + 1
        data[, 'restecg'] <- data[, 'restecg'] + 1
        data[, 'thal'] <- data[, 'thal'] + 1
        
        return (data)
    }
    
    removeNa <- function(data, cols) {
        data <- data[complete.cases(data[, cols]),]
        return (data)
    }
    
    retVal <- list(cleveland.data = removeNa(shiftFactors(cleveland.data), c('age', 'sex', 'chol')),
                   hungarian.data = removeNa(shiftFactors(hungarian.data), c('age', 'sex', 'chol')),
                   switzerland.data = removeNa(shiftFactors(switzerland.data), c('age', 'sex', 'chol')),
                   va.data = removeNa(shiftFactors(va.data), c('age', 'sex', 'chol')))
    
    return (retVal) 
}



loadDataFromFile <- function(file, use.factors = TRUE, use.booleans = TRUE) {
    # Load the heart disease data from the given file
    # and do some minor processing to make the features consistent.
    #
    # Args:
    #   file: The path to the heart disease data set to load.
    #   use.factors: If false then factors will be converted to numbers
    #   use.booleans: If false then booleans/logical will be converted to numbers
    #
    # Returns:
    #   A data frame containing the heart disease data for the given file
    
    
    # Create column names and classes that match the original data set.
    # See https://archive.ics.uci.edu/ml/datasets/Heart+Disease
    # All factors that are number strings need to be made numbers first because
    # the various data files use mixed decimals and integers which are different factor levels
    # (e.g., '1.0' is different than '1')
    col_info <- rbind(
        c('age', 'real'),
        c('sex', 'real'),           #This will become factor
        c('cp', 'real'),            #This will become factor
        c('trestbps','real'),
        c('chol', 'real'),
        c('fbs', 'real'),           #This will become bool
        c('restecg', 'real'),       #This will become factor
        c('thalach', 'real'),
        c('exang', 'real'),         #This will become bool
        c('oldpeak', NA), #TODO
        c('slope', 'real'),         #This will become factor
        c('ca', 'real'),
        c('thal', 'real'),          #This will become factor
        c('num', 'real')            #This will become bool
    )
    
    # Load the preprocessed 
    data_orig <- read.csv2(file, header = FALSE, sep = ',', col.names = col_info[, 1], colClasses = col_info[, 2],  dec = '.', na.strings = c('?'))
    
    # Make factors
    if (use.factors) {
        data_orig[, 'sex'] <- as.factor(data_orig[, 'sex'])
        data_orig[, 'cp'] <- as.factor(data_orig[, 'cp'])
        data_orig[, 'restecg'] <- as.factor(data_orig[, 'restecg'])
        data_orig[, 'slope'] <- as.factor(data_orig[, 'slope'])
        data_orig[, 'thal'] <- as.factor(data_orig[, 'thal'])
    }
    
    # Make the 'num' col 0 or 1 (values 1-4 are all true accoring to data info)
    # These will get converted to bool below
    data_orig[data_orig[, 'num'] > 0 , 'num'] <-  1
    
    # This is a helper function to convert number vector to logical vector
    # (while preserving values that are not true or false)
    convert_to_bool <- function(data_vector, true_value, false_value) {
        true_indicies <- data_vector == true_value
        false_indicies <- data_vector == false_value
        new_data <- rep(NA, length(data_vector))
        new_data[true_indicies] <- TRUE
        new_data[false_indicies] <- FALSE
        return (new_data)
    }
    
    # Make bool values
    if (use.booleans) {
        data_orig[, 'fbs'] <- convert_to_bool(data_orig[, 'fbs'], 1, 0)
        data_orig[, 'exang'] <- convert_to_bool(data_orig[, 'exang'], 1, 0)
        data_orig[, 'num'] <- convert_to_bool(data_orig[, 'num'], 1, 0)
    }
    
    # Set missing cholesterol values to NA
    chol_na <- data_orig[, "chol"] < 1 | is.na(data_orig[, "chol"]) | is.null(data_orig[, "chol"])
    data_orig[chol_na ,"chol"] <- NA

    return (data_orig)
}



makeJagsData <- function(processed.data) {
    # This will take the output of processData() function
    # and create a numeric (m x n x o) array of data.
    #
    # Args:
    #   processed.data: the data returned by the processData() function. (list of data frames)
    #
    # Returns:
    #   An (m x n x o) array. the m dim is the database, the n dim is the age group, and the o dim is the column.
    
    db.count <- length(processed.data)
    row.count <- nrow(processed.data[[1]])
    col.count <- ncol(processed.data[[1]])
    
    ret.val <- array(data = 0, dim = c(db.count, row.count, col.count))
    
    
    # These need to match the processed.data + the new column (note: only use numeric cols)
    col.names <- c('min.age',
                   'max.age',
                   'avg.age',
                   'lc.cases',
                   'lc.patients',
                   'hc.cases',
                   'hc.patients',
                   'avg.age.scaled')
    
    for (db.index in 1:db.count) {
        
        # Add average age scaled and centered
        avg.age <- as.numeric(processed.data[[db.index]]$avg.age)
        processed.data[[db.index]]$avg.age.scaled  <- as.vector(scale(avg.age, scale=2*sd(avg.age)))
        
        db.matrix <- as.matrix(processed.data[[db.index]][,col.names])
        ret.val[db.index,,] <- db.matrix
    }
    
    return (ret.val)
}



makeJagsDataAlt <- function(processed.data) {
    # This will take the output of processData() function
    # and create a numeric (m x n x o) array of data.
    #
    # Args:
    #   processed.data: the data returned by the processData() function. (list of data frames)
    #
    # Returns:
    #   An (m x n x o) array. the m dim is the database, the n dim is the age group, and the o dim is the column.
    
    db.count <- length(processed.data)
    row.count <- nrow(processed.data[[1]])
    col.count <- ncol(processed.data[[1]])
    
    ret.val <- array(data = 0, dim = c(db.count, row.count, col.count))
    
    
    # These need to match the processed.data + the new column (note: only use numeric cols)
    col.names <- c('min.age',
                   'max.age',
                   'avg.age',
                   'cases',
                   'patients',
                   'avg.age.scaled')
    
    for (db.index in 1:db.count) {
        
        # Add average age scaled and centered
        avg.age <- as.numeric(processed.data[[db.index]]$avg.age)
        processed.data[[db.index]]$avg.age.scaled  <- as.vector(scale(avg.age, scale=2*sd(avg.age)))
        
        db.matrix <- as.matrix(processed.data[[db.index]][,col.names])
        ret.val[db.index,,] <- db.matrix
    }
    
    return (ret.val)
}


makeMonitorNames <- function(name, m, n = NULL, o = NULL) {
    # This will make the names of the JAGS sample outputs
    # when the output is a matrix. E.g.,  "beta[1,1,1]", "beta[1,1,2]", ....
    #
    # Args:
    #   name: the name of the variable
    #   m: the length of the first dimension
    #   n: the length of the second dimension (leave out for a one dimensionl vector)
    #   o: the length of the third dimension (leave out for a 1 or 2 dimensional input)
    #
    # Output:
    #   A vector of names like "beta[1,1]", "beta[1,2]", ....
    
    if (is.null(n)) {
        
        out <- rep('', m )
        out.index <- 1
        for (m.index in 1:m) {
            out[out.index] <- paste(name, '[', m.index, ']', sep = '')
            out.index =  out.index + 1
        }
        return (out)
    } else if (is.null(o)) {
        out <- rep('', m * n)
        out.index <- 1
        for (m.index in 1:m) {
            for (n.index in 1:n) {
                out[out.index] <- paste(name, '[', m.index, ',', n.index, ']', sep = '')
                out.index =  out.index + 1
            }
        }
        return (out)
    } else {
        out <- rep('', m * n * 0)
        out.index <- 1
        for (m.index in 1:m) {
            for (n.index in 1:n) {
                for (o.index in 1:o) {
                    out[out.index] <- paste(name, '[', m.index, ',', n.index, ',', o.index, ']', sep = '')
                    out.index =  out.index + 1
                }
            }
        }
        return (out)
    }
}



plotResults <- function(sample.results) {
    # Plot the results (posterior probabilities).
    #
    # Args:
    #   sample.results: output from extractResultsModel1() function.
    #
    # Returns:
    #   none
    
    db.count <- length(sample.results)
    
    legend.names <- rep('', 2*db.count)
    
    for (db.index in 1:db.count) {
        
        legend.names[(db.index - 1)*2 + 1] <- paste('low chol: database ', db.index)
        legend.names[(db.index - 1)*2 + 2] <- paste('high chol: database ', db.index)
        
        x <- sample.results[[db.index]]$avg.age
        y.lc <- sample.results[[db.index]]$lc.prob
        y.hc <- sample.results[[db.index]]$hc.prob
        
        if (db.index == 1) {
            plot( x = x, y = y.lc, col=db.index, ylim=c(0,1), type = 'l', lty = 2, ylab = "Prob of Heart disease", xlab = "Age")
        } else {
            lines(x = x, y = y.lc, col=db.index, type = 'l', lty = 2)
        }
        
        lines(x = x, y = y.hc, col=db.index)
    }
    
    legend('topleft', legend = legend.names, col = c(1,1,2,2,3,3), lty = c(2,1))
}

plotResultsAlt <- function(sample.results) {
    # Plot the results (posterior probabilities).
    #
    # Args:
    #   sample.results: output from extractResultsModelAlt() function.
    #
    # Returns:
    #   none
    
    db.count <- length(sample.results)
    
    legend.names <- rep('', db.count)
    
    for (db.index in 1:db.count) {
        
        legend.names[db.index]  <- paste('database ', db.index)
        
        x <- sample.results[[db.index]]$avg.age
        y <- sample.results[[db.index]]$prob
        
        if (db.index == 1) {
            plot( x = x, y = y, col=db.index, ylim=c(0,1), type = 'l', lty = 2, ylab = "Prob of Heart disease", xlab = "Age")
        } else {
            lines(x = x, y = y, col=db.index, type = 'l', lty = 2)
        }
    }
    
    legend('topleft', legend = legend.names, col = c(1,2,3), lty = c(2))
}


processData <- function(loaded.data, age.interval) {
    # This function will take the intital loaded data and create 
    # and separate it it into age groups. The databases will remain separate
    #
    # Args:
    #   loaded.data: the list of data ad returned by the loadData() function.
    #   age.interval: the range of ages to use for each group.
    #
    # Returns:
    #   A list of data frames where the data is grouped by age and cholesterol.
    
    
    # Get the database names
    db.names <- names(loaded.data)
    
    # Get the age and cholesterol levels across all data
    all.cholesterol = c()
    all.ages = c()
    for (i in 1:length(loaded.data)) {
        all.cholesterol <- c(all.cholesterol, loaded.data[[i]]$chol)
        all.ages <- c(all.ages, loaded.data[[i]]$age)
    }
    
    
    # Get the median cholesterol using all data
    median.chol <- quantile(all.cholesterol, 0.5)
    
    # Get age information across all data
    min.age <- min(all.ages)
    max.age <- max(all.ages)
    
    
    # This is a little helper function to combine add rows to a data frame
    addRowToDataFrame <- function (df = NULL, row) {
        
        col.names <- c('db.name',
                      'min.age',
                      'max.age',
                      'avg.age',
                      'lc.cases',
                      'lc.patients',
                      'hc.cases',
                      'hc.patients')
        
        empty.df <- data.frame(db.name = character(),
                         min.age = double(),
                         max.age = double(),
                         avg.age = double(),
                         lc.cases = double(),
                         lc.patients = double(),
                         hc.cases = double(),
                         hc.patients = double())
        
        colnames(empty.df) <- col.names
        
        if (is.null(df)) {
            df <- empty.df
        }
        
        df <- rbind(df, row)
        colnames(df) <- col.names
        
        return (df)
    }
    
    
    # Store the output data
    ret.val <- list()
    
    for (db.index in 1:length(loaded.data)) {
        
        db.data <- loaded.data[[db.index]]
        db.name <- db.names[db.index]
        db.df <- NULL

        # Group by age
        current.min.age <- min.age
        current.max.age  <- current.min.age + age.interval
        age.group.index <- 1
        
        while (current.min.age <= max.age) { 
            
            age.group.data <- subset(db.data, age >= current.min.age & age <= current.max.age)
            
            lc.subset <- subset(age.group.data, chol < median.chol)
            hc.subset <- subset(age.group.data, chol >= median.chol)

            row <- list(db.name,
                        current.min.age,
                        current.max.age,
                        mean(c(current.max.age, current.min.age)),
                        sum(lc.subset$num > 0),
                        nrow(lc.subset),
                        sum(hc.subset$num > 0),
                        nrow(hc.subset))
            
            
            db.df <- addRowToDataFrame(db.df, row)
            
            # Update the age interval
            current.min.age <- current.max.age + 1
            current.max.age <- current.min.age + age.interval
        }
        
        # Don't add unless there are cases
        total.lc.cases <- sum(db.df$lc.cases)
        total.hc.cases <- sum(db.df$hc.cases)
        
        if (total.lc.cases > 0 & total.hc.cases > 0) {
            ret.val[[length(ret.val) + 1]] <- db.df
        }
    }
    
    return (ret.val)
}



processDataAlt <- function(loaded.data, age.interval) {
    # This function will take the intital loaded data and create 
    # and separate it it into age groups. The databases will remain separate
    #
    # Args:
    #   loaded.data: the list of data ad returned by the loadData() function.
    #   age.interval: the range of ages to use for each group.
    #
    # Returns:
    #   A list of data frames where the data is grouped by age.
    
    
    # Get the database names
    db.names <- names(loaded.data)
    
    # Get the ages and across all data
    all.ages = c()
    for (i in 1:length(loaded.data)) {
        all.ages <- c(all.ages, loaded.data[[i]]$age)
    }
        
    # Get age information across all data
    min.age <- min(all.ages)
    max.age <- max(all.ages)
    
    
    # This is a little helper function to combine add rows to a data frame
    addRowToDataFrame <- function (df = NULL, row) {
        
        col.names <- c('db.name',
                      'min.age',
                      'max.age',
                      'avg.age',
                      'cases',
                      'patients')
        
        empty.df <- data.frame(db.name = character(),
                         min.age = double(),
                         max.age = double(),
                         avg.age = double(),
                         cases = double(),
                         patients = double())
        
        colnames(empty.df) <- col.names
        
        if (is.null(df)) {
            df <- empty.df
        }
        
        df <- rbind(df, row)
        colnames(df) <- col.names
        
        return (df)
    }
    
    
    # Store the output data
    ret.val <- list()
    
    for (db.index in 1:length(loaded.data)) {
        
        db.data <- loaded.data[[db.index]]
        db.name <- db.names[db.index]
        db.df <- NULL

        # Group by age
        current.min.age <- min.age
        current.max.age  <- current.min.age + age.interval
        age.group.index <- 1
        
        while (current.min.age <= max.age) { 
            
            age.group.data <- subset(db.data, age >= current.min.age & age <= current.max.age)
            

            row <- list(db.name,
                        current.min.age,
                        current.max.age,
                        mean(c(current.max.age, current.min.age)),
                        sum(age.group.data$num > 0),
                        nrow(age.group.data))
            
            
            db.df <- addRowToDataFrame(db.df, row)
            
            # Update the age interval
            current.min.age <- current.max.age + 1
            current.max.age <- current.min.age + age.interval
        }
        
        # Don't add unless there are cases
        total.cases <- sum(db.df$cases)

        
        if (total.cases > 0) {
            ret.val[[length(ret.val) + 1]] <- db.df
        }
    }
    
    return (ret.val)
}

runModel <- function(model, sample.iterations, thin, sample) {
    # This will run the model and return the samples. 
    #
    # Args:
    #   model: the model returned by jags.model function
    #   sample.iterations: the number of iterations to run
    #   thin: the thinning interval
    #   sample: a vector of parameters to sample
    #
    # Returns:
    #   The coda samples.
    
    x1 <- coda.samples(model, sample, thin = thin, n.iter = sample.iterations)
    return (x1)
}








main <- function() {
    init()

    # This will load all data with some light preprocessing.
    # The results os a list of data frames (one for each database.)
    loaded.data <- loadAllData()

    # This will split the data in each data frame into age groups where each
    # age group contains the number of cases of heart disease and total
    # patients for low cholesterol and high cholesterol group. I.e., each age
    # is further split into those with high and low cholesterol and the the number
    # of heart disease cases for each group is calculated. age.interval is the number of years in each age group, e.g., an age interval of 5 will give group 30-35,36-41, ...
    age.interval <- 3
    processed.data <- processData(loaded.data, age.interval)


    # This will take the processed.data and create a multidimensional array.
    # The array will be (m x n x o) where m is the database, n is the age group,
    # and o is the data in the age group. This data is passed to the JAGS model.
    jags.input.data <- makeJagsData(processed.data)

    #----------------------------------------------------------------------
    # Run the model
    #----------------------------------------------------------------------

    # Run the model adaptation and initial burn in.
    model <- initJagsModel1(jags.input.data )

    # A vector of parameters to monitor.
    monitor <- c('prob', 'beta.age', 'beta.chol', 'sigmaepsilon', 'epsilon', 'rep.data')

    # Burn-in
    burn.in.iterations <- 50000
    thinning <- 10
    samples <- runModel(model, burn.in.iterations, thinning, monitor)

    # Sample
    samples <- runModel(model, 10000, 1, monitor)

    #----------------------------------------------------------------------
    # Check convergence
    #----------------------------------------------------------------------
    gelman.vars <-  c(makeMonitorNames('beta.age', 3),
                      makeMonitorNames('beta.chol', 3, 2),
                      makeMonitorNames('epsilon', 3, 13, 2))
    gelman.diag(samples[, gelman.vars], autoburnin=FALSE, multivariate = FALSE)
    gelman.plot(samples[, gelman.vars], autoburnin = FALSE)


    #----------------------------------------------------------------------
    # Check effective sample size
    #----------------------------------------------------------------------
    effectiveSize(samples[, gelman.vars])

    #----------------------------------------------------------------------
    # Check chi-square discrepency
    #----------------------------------------------------------------------    
    # Take the coda samples and extract some data from them (e.g., posterior probs and
    # replicate data)
    sample.results <- extractResultsModel1(samples, processed.data)

    # Get the posterior probabilties and replicate data for each draw
    draws <- extractDrawsModel1(sample.results, samples)

    # Get posterior predictive p-values for chi-square discrepency measure
    T.chi.tests <- getTChiSqr(sample.results, draws)
    print(T.chi.tests)

    #----------------------------------------------------------------------
    # Plot the results
    #---------------------------------------------------------------------- 
    plotResults(sample.results)
}