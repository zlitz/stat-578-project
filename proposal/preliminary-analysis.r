load_heart_data <- function(file) {
    # Create column names and classes that match the original data set.
    # See https://archive.ics.uci.edu/ml/datasets/Heart+Disease
    # All factors that are number strings need to be made numbers first because
    # the various data files use mixed decimals and integers which are different factor levels
    # (e.g., '1.0' is different than '1')
    col_info <- rbind(
        c('age', 'real'),
        c('sex', 'real'),           #This will become factor
        c('cp', 'real'),            #This will become factor
        c('trestbps','real'),
        c('chol', 'real'),
        c('fbs', 'real'),           #This will become bool
        c('restecg', 'real'),       #This will become factor
        c('thalach', 'real'),
        c('exang', 'real'),         #This will become bool
        c('oldpeak', NA), #TODO
        c('slope', 'real'),         #This will become factor
        c('ca', 'real'),
        c('thal', 'real'),          #This will become factor
        c('num', 'real')            #This will become bool
    )

    # Load the preprocessed 
    data_orig <- read.csv2(file, header = FALSE, sep = ',', col.names = col_info[, 1], colClasses = col_info[, 2],  dec = '.', na.strings = c('?'))
    
    # Make factors
    data_orig[, 'sex'] <- as.factor(data_orig[, 'sex'])
    data_orig[, 'cp'] <- as.factor(data_orig[, 'cp'])
    data_orig[, 'restecg'] <- as.factor(data_orig[, 'restecg'])
    data_orig[, 'slope'] <- as.factor(data_orig[, 'slope'])
    data_orig[, 'thal'] <- as.factor(data_orig[, 'thal'])
    
    # Make the 'num' col 0 or 1 (values 1-4 are all true accoring to data info)
    # These will get converted to bool below
    data_orig[data_orig[, 'num'] > 0 , 'num'] <-  1
    
    # This is a helper function to convert number vector to logical vector
    # (while preserving values that are not true or false)
    convert_to_bool <- function(data_vector, true_value, false_value) {
        true_indicies <- data_vector == true_value
        false_indicies <- data_vector == false_value
        new_data <- rep(NA, length(data_vector))
        new_data[true_indicies] <- TRUE
        new_data[false_indicies] <- FALSE
        return (new_data)
    }
    
    # Make bool values
    data_orig[, 'fbs'] <- convert_to_bool(data_orig[, 'fbs'], 1, 0)
    data_orig[, 'exang'] <- convert_to_bool(data_orig[, 'exang'], 1, 0)
    data_orig[, 'num'] <- convert_to_bool(data_orig[, 'num'], 1, 0)

    return (data_orig)
}



#--------------------------------------------------
# Begin by loading and processing the data
#--------------------------------------------------

# This will load the data, add column names, and set column types
cleveland_data <- load_heart_data('data/processed.cleveland.data')
hungarian_data <- load_heart_data('data/processed.hungarian.data')
switzerland_data <- load_heart_data('data/processed.switzerland.data')
va_data <- load_heart_data('data/processed.va.data')

# Combine the data sets into a single set
total_data <- rbind(cleveland_data, hungarian_data, switzerland_data, va_data)

